from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
	path('aboutMe', views.index2, name='index2'),
	path('help', views.index3, name='index3'),
	path('register', views.index4, name='index4'),
]